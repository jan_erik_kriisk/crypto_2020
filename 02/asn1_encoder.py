#!/usr/bin/env python
import sys  # do not use any other imports/libraries


# took 7.5 hours (please specify here how much time your solution required)


def nb(i):
    # i - integer to encode as bytes
    # Create bytes object
    b = bytes()
    # Add all the bytes
    while i != 0:
        byte = i & 0xff
        b = bytes([byte]) + b
        i = i >> 8
    return b


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    if len(value_bytes) < 128:
        return bytes([len(value_bytes)])
    else:
        lenBytes = nb(len(value_bytes))
        padding = nb(128 | len(lenBytes))
        return padding + lenBytes


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'
    return bytes([0x01]) + asn1_len(bool) + bool


def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    integer = nb(i)
    # If integer is zero add a zero byte
    if i == 0:
        integer = bytes([0x00])
    # If the integers first byte starts with a 1 bit, add a zero byte for padding
    elif integer[0] >> 7 == 1:
        integer = bytes([0x00]) + integer
    return bytes([2]) + asn1_len(integer) + integer


def asn1_bitstring(bitstr):
    # bitstr - string containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    padding = 0
    # Add zeroes until bitstr is 1 byte
    while len(bitstr) % 8 != 0:
        bitstr += "0"
        padding += 1
    i = 0
    # Flag is added to know when the padding ends and system function needs to be changed
    flag = True
    # If we get 8 zero bits in the padding than we mark it as 1 zero byte
    zeros = 0
    zeroBytes = 0
    # Convert bitstr to integer
    for bit in bitstr:
        i <<= 1
        # If the padding ends we stop counting the zero bits
        if bit == "1":
            i |= 1
            flag = False
        elif flag:
            zeros += 1
            if zeros == 8:
                zeroBytes += 1
                zeros = 0
    # We put all of the necessary padding zero bytes into the value byte
    paddingStr = bytes([padding]) + b'\x00' * zeroBytes + nb(i)
    return bytes([3]) + asn1_len(paddingStr) + paddingStr


def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([4]) + asn1_len(octets) + octets


def asn1_null():
    # returns DER encoding of NULL
    return bytes([5]) + bytes([0])


def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    # Add the first 2 components
    value = nb(40 * oid[0] + oid[1])
    # Add the rest of the components
    for i in range(len(oid) - 2):
        integer = oid[i + 2]
        # For each component we start from the last byte, for the last byte the first left most bit is 0
        b = bytes()
        byte = (integer & 127)
        b = bytes([byte]) + b
        integer = integer >> 7
        # We encode the rest of the bytes and for each the left most bit will be 1
        while integer != 0:
            byte = (integer & 127)
            byte |= 128
            b = bytes([byte]) + b
            integer = integer >> 7
        value += b
    return bytes([6]) + asn1_len(value) + value


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([48]) + asn1_len(der) + der


def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    return bytes([49]) + asn1_len(der) + der


def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    return bytes([19]) + asn1_len(string) + string


def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    return bytes([23]) + asn1_len(time) + time


def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    if tag <= 30:
        tag = tag & 0x1f
    return bytes([160 | tag]) + asn1_len(der) + der


# figure out what to put in '...' by looking on ASN.1 structure required (see slides)
asn1 = asn1_tag_explicit(asn1_sequence(asn1_set(asn1_integer(5) + asn1_tag_explicit(asn1_integer(200), 2) +
                                                asn1_tag_explicit(asn1_integer(65407), 11)) +
                                       asn1_boolean(True) + asn1_bitstring("110") +
                                       asn1_octetstring(b'\x00' + b'\x01' + b'\x02' * 49)
                                       + asn1_null() + asn1_objectidentifier([1, 2, 840, 113549, 1]) +
                                       asn1_printablestring(b'hello.') +
                                       asn1_utctime(b'150223010900Z')), 0)
open(sys.argv[1], 'wb').write(asn1)
