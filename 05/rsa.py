#!/usr/bin/env python3

import codecs, hashlib, os, sys  # do not use any other imports/libraries
from pyasn1.codec.der import decoder


# took 9.5 hours (please specify here how much time your solution required)


def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length==False:
        length = (i.bit_length()+7)//8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b


def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    if len(value_bytes) < 128:
        return bytes([len(value_bytes)])
    else:
        lenBytes = nb(len(value_bytes))
        padding = nb(128 | len(lenBytes))
        return padding + lenBytes


def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([4]) + asn1_len(octets) + octets


def asn1_null():
    # returns DER encoding of NULL
    return bytes([5]) + bytes([0])


def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    # Add the first 2 components
    value = nb(40 * oid[0] + oid[1])
    # Add the rest of the components
    for i in range(len(oid) - 2):
        integer = oid[i + 2]
        # For each component we start from the last byte, for the last byte the first left most bit is 0
        b = bytes()
        byte = (integer & 127)
        b = bytes([byte]) + b
        integer = integer >> 7
        # We encode the rest of the bytes and for each the left most bit will be 1
        while integer != 0:
            byte = (integer & 127)
            byte |= 128
            b = bytes([byte]) + b
            integer = integer >> 7
        value += b
    return bytes([6]) + asn1_len(value) + value


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([48]) + asn1_len(der) + der


def pem_to_der(content):
    # converts PEM content to DER
    content = content[1:-1]
    text = bytes()
    for line in content:
        text += line
    return codecs.decode(text, 'base64')


def get_pubkey(filename):
    # Reads public key file and returns (n, e)
    content = open(filename, "rb").readlines()
    if content[0] == b'-----BEGIN PUBLIC KEY-----\n' and\
            content[-1] == b'-----END PUBLIC KEY-----\n':
        content = pem_to_der(content)
    else:
        text = bytes()
        for line in content:
            text += line
        content = text
    # decode the DER to get public key DER structure, which is encoded as BITSTRING
    bitstring = decoder.decode(content)[0][1]
    # convert BITSTRING to bytestring
    i = 0
    for bit in str(bitstring):
        i <<= 1
        if bit == '1':
            i |= 1
    bytestring = nb(i)
    # decode the bytestring (which is actually DER) and return (n, e)
    pubkey = decoder.decode(bytestring)[0]
    return int(pubkey[0]), int(pubkey[1])


def get_privkey(filename):
    # Reads private key file and returns (n, d)
    content = open(filename, "rb").readlines()
    if content[0] == b'-----BEGIN RSA PRIVATE KEY-----\n' and\
            content[-1] == b'-----END RSA PRIVATE KEY-----\n':
        content = pem_to_der(content)
    else:
        text = bytes()
        for line in content:
            text += line
        content = text
    # Decode the file
    privkey = decoder.decode(content)
    return int(privkey[0][1]), int(privkey[0][3])


def pkcsv15pad_encrypt(plaintext, n):
    # pad plaintext for encryption according to PKCS#1 v1.5
    # calculate number of bytes required to represent the modulus n
    nr = len(nb(n)) - (11 + len(plaintext))
    # plaintext must be at least 11 bytes smaller than the modulus
    if len(plaintext) >= n - 11:
        print("[-] Plaintext is too large")
        pass
    # generate padding bytes
    ps = bytes()
    counter = 0
    while True:
        if counter == 8 + nr:
            break
        byte = os.urandom(1)
        if byte != b'\x00':
            ps += byte
            counter += 1
    padded_plaintext = b'\x00' + b'\x02' + ps + b'\x00' + plaintext
    return padded_plaintext


def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5
    # calculate byte length of modulus n
    nr = len(nb(n)) - (3 + len(plaintext))
    # plaintext must be at least 3 bytes smaller than the modulus
    if len(plaintext) >= n - 3:
        print("[-] Plaintext is too large")
        pass
    # generate padding bytes
    padded_plaintext = b'\x00' + b'\x01' + b'\xFF' * nr + b'\x00' + plaintext
    return padded_plaintext


def pkcsv15pad_remove(plaintext):
    # removes PKCS#1 v1.5 padding
    padded_plaintext = plaintext[1:]
    counter = 1
    newtext = bytes()
    for byte in padded_plaintext:
        if byte == 0:
            newtext = padded_plaintext[counter:]
            break
        else:
            counter += 1
    return newtext


def encrypt(keyfile, plaintextfile, ciphertextfile):
    plaintext = open(plaintextfile, 'rb').read()
    pubkey = get_pubkey(keyfile)
    n = pubkey[0]
    e = pubkey[1]
    padded_plaintext = pkcsv15pad_encrypt(plaintext, n)
    m = bn(padded_plaintext)
    c = nb(pow(m, e, n))
    cipherfile = open(ciphertextfile, "wb")
    cipherfile.write(c)
    cipherfile.close()
    pass


def decrypt(keyfile, ciphertextfile, plaintextfile):
    ciphertext = open(ciphertextfile, 'rb').read()
    privkey = get_privkey(keyfile)
    n = privkey[0]
    d = privkey[1]
    c = bn(ciphertext)
    m = nb(pow(c, d, n))
    p = pkcsv15pad_remove(m)
    plaintext = open(plaintextfile, 'wb')
    plaintext.write(p)
    plaintext.close()
    pass


def digestinfo_der(filename):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA256 digest of file
    file = open(filename, 'rb')
    digestInfo = hashlib.sha3_256(file.read()).digest()
    asn1 = asn1_sequence(
        asn1_sequence(
            asn1_objectidentifier([2, 16, 840, 1, 101, 3, 4, 2, 1]) +
            asn1_null()
        ) +
        asn1_octetstring(digestInfo)
    )
    file.close()
    return asn1


def sign(keyfile, filetosign, signaturefile):
    der = digestinfo_der(filetosign)
    file = open(filetosign, 'wb')
    file.write(der)
    file.close()
    privkey = get_privkey(keyfile)
    n = privkey[0]
    d = privkey[1]
    padded_plaintext = pkcsv15pad_sign(der, n)
    m = bn(padded_plaintext)
    # Warning: make sure that signaturefile produced has the same
    # length as the modulus (hint: use parametrized nb()).
    c = nb(pow(m, d, n), len(nb(n)))
    signature = open(signaturefile, "wb")
    signature.write(c)
    signature.close()
    pass


def verify(keyfile, signaturefile, filetoverify):
    signature = open(signaturefile, 'rb').read()
    pubkey = get_pubkey(keyfile)
    n = pubkey[0]
    e = pubkey[1]
    s = bn(signature)
    m = nb(pow(s, e, n))
    p = pkcsv15pad_remove(m)
    digest = decoder.decode(p)[0][1]
    verificationFile = open(filetoverify, 'rb').read()
    verification = decoder.decode(verificationFile)[0][1]
    # Prints "Verified OK" or "Verification failure"
    if digest == verification:
        print("Verified OK")
    else:
        print("Verification Failure")
    pass


def usage():
    print("Usage:")
    print("encrypt <public key file> <plaintext file> <output ciphertext file>")
    print("decrypt <private key file> <ciphertext file> <output plaintext file>")
    print("sign <private key file> <file to sign> <signature output file>")
    print("verify <public key file> <signature file> <file to verify>")
    sys.exit(1)


if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'sign':
    sign(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'verify':
    verify(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()
