#!/usr/bin/env python3

import argparse, codecs, datetime, os, socket, sys, time  # do not use any other imports/libraries
from urllib.parse import urlparse

# took 3.5 hours (please specify here how much time your solution required)

# parse arguments
parser = argparse.ArgumentParser(description='TLS v1.2 client')
parser.add_argument('url', type=str, help='URL to request')
parser.add_argument('--certificate', type=str, help='File to write PEM-encoded server certificate')
args = parser.parse_args()


def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length == False:
        length = (i.bit_length() + 7) // 8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b


def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i


# returns TLS record that contains ClientHello Handshake message
def client_hello():
    print("--> ClientHello()")

    # list of cipher suites the client supports
    csuite = b"\x00\x05"  # TLS_RSA_WITH_RC4_128_SHA
    csuite += b"\x00\x2f"  # TLS_RSA_WITH_AES_128_CBC_SHA
    csuite += b"\x00\x35"  # TLS_RSA_WITH_AES_256_CBC_SHA

    # add Handshake message header
    hbody = b'\x03\x03' + nb(int(time.time()), 4) + os.urandom(28) + b'\x00' + nb(len(csuite), 2) + csuite + b'\x01\x00'
    hheader = b'\x01' + nb(len(hbody), 3) + hbody

    # add record layer header
    rheader = b'\x16' + b'\x03\x03' + nb(len(hheader), 2)
    return rheader + hheader


# returns TLS record that contains 'Certificate unknown' fatal Alert message
def alert():
    print("--> Alert()")
    # add alert message
    # add record layer header
    return b'\x15' + b'\x03\x03' + b'\x00\x02' + b'\x02' + b'\x2E'


# parse TLS Handshake messages
def parsehandshake(r, htype):
    global server_hello_done_received
    print(" <--- Handshake()")
    # read Handshake message type and length from message header
    if r[0] == 0x02 or r[0] == 0x0b or r[0] == 0x0e:
        htype = r[0]
    if htype == 0x02:
        print("     <--- ServerHello()")
        gmt = r[6:10]
        server_random = r[10:38]
        idlen = r[38]
        sessid = r[39:39 + idlen]
        print("	[+] server randomness:", server_random.hex().upper())
        print("	[+] server timestamp:", datetime.datetime.fromtimestamp(bn(gmt)).strftime('%Y-%m-%d %H:%M:%S'))
        print("	[+] TLS session ID:", sessid.hex().upper())
        cipher = r[39 + idlen: 39 + idlen + 2]
        if cipher == b"\x00\x2f":
            print("	[+] Cipher suite: TLS_RSA_WITH_AES_128_CBC_SHA")
        elif cipher == b"\x00\x35":
            print("	[+] Cipher suite: TLS_RSA_WITH_AES_256_CBC_SHA")
        elif cipher == b"\x00\x05":
            print("	[+] Cipher suite: TLS_RSA_WITH_RC4_128_SHA")
        else:
            print("[-] Unsupported cipher suite selected:", cipher.hex())
            sys.exit(1)
        compression = r[39 + idlen + 2]
        if compression != 0x00:
            print("[-] Wrong compression:", compression)
            sys.exit(1)
    elif htype == 0x0b:
        if r[0] == 0x0b:
            certlen = bn(r[7:10])
        else:
            certlen = bn(r[0:3])
        print("     <--- Certificate()")
        print("	[+] Server certificate length:", certlen)
        if args.certificate:
            print("	[+] Server certificate saved in:", args.certificate)
            f = open(args.certificate, "wb")
            f.write(b'-----BEGIN TRUSTED CERTIFICATE-----\n')
            f.write(codecs.encode(r[10:10 + certlen], 'base64'))
            f.write(b'-----END TRUSTED CERTIFICATE-----\n')
            f.close()
            return
    elif htype == 0x0e:
        print("     <--- ServerHelloDone()")
        server_hello_done_received = True
    else:
        print("[-] Unknown Handshake type:", htype)
        sys.exit(1)

    # handle the case of several handshake messages in one record
    if htype == 0x02:
        leftover = r[39 + idlen + 3:]
    elif htype == 0x0b:
        if r[0] == 0x0b:
            leftover = r[10 + certlen:]
        else:
            leftover = r[3 + certlen:]
    elif htype == 0x0e:
        return
    if len(leftover):
        parsehandshake(leftover, htype)


# parses TLS record
def parserecord(r):
    # parse TLS record header and pass the record body to the corresponding parsing method (i.e., parsehandshake())
    parsehandshake(r, None)


# read from the socket full TLS record
def readrecord():
    global s
    record = b""
    # read the TLS record header (5 bytes)
    firstBytes = b""
    for i in range(5):
        firstBytes += s.recv(1)
    # find data length
    if len(firstBytes) == 5:
        length = firstBytes[3:]
    else:
        print("[-] Incorrect amount of bytes received")
        sys.exit(1)
    # read the TLS record body
    for i in range(bn(length)):
        record += s.recv(1)
    return record


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
url = urlparse(args.url)
host = url.netloc.split(':')
if len(host) > 1:
    port = int(host[1])
else:
    port = 443
host = host[0]
path = url.path
s.connect((host, port))
s.send(client_hello())

server_hello_done_received = False
while not server_hello_done_received:
    parserecord(readrecord())
s.send(alert())

print("[+] Closing TCP connection!")
s.close()
