#!/usr/bin/env python3
import os, sys  # do not use any other imports/libraries


# took 4.0 hours 

def bn(b):
    # b - bytes to encode as integer
    # Add the first byte
    i = b[0]
    # Add rest of the bytes
    for j in range(len(b) - 1):
        i = i << 8
        i = i | b[j + 1]
    return i


def nb(i, length):
    # i - integer to encode as bytes
    # length - specifies in how many bytes the number should be encoded
    # Create bytes object
    b = bytes()
    # Add all the bytes
    for j in range(length):
        byte = i >> (8 * j) & 0xff
        b = bytes([byte]) + b
    return b


def encrypt(pfile, kfile, cfile):
    # Read file
    plainBytes = open(pfile, "rb").read()
    # Convert file bytes to integer
    plainInt = bn(plainBytes)
    # Generate random key
    randomKey = os.urandom(len(plainBytes))
    # Convert key bytes to integer
    keyInt = bn(randomKey)
    # XOR operation
    cipherInt = plainInt ^ keyInt
    # Convert key integer to bytes
    keyBytes = nb(keyInt, len(randomKey))
    # Write key to file
    keyFile = open(kfile, "wb")
    keyFile.write(keyBytes)
    keyFile.close()
    # Convert cipher integer to bytes
    cipherBytes = nb(cipherInt, len(plainBytes))
    # Write cipher to file
    plainFile = open(cfile, "wb")
    plainFile.write(cipherBytes)
    plainFile.close()
    pass


def decrypt(cfile, kfile, pfile):
    # Read files
    encBytes = open(cfile, "rb").read()
    keyBytes = open(kfile, "rb").read()
    # Convert file bytes to integers
    encInt = bn(encBytes)
    keyInt = bn(keyBytes)
    # XOR operation
    cipherInt = encInt ^ keyInt
    # Convert cipher integer to bytes
    cipherBytes = nb(cipherInt, len(encBytes))
    # Write cipher to file
    plainFile = open(pfile, "wb")
    plainFile.write(cipherBytes)
    plainFile.close()
    pass


def usage():
    print("Usage:")
    print("encrypt <plaintext file> <output key file> <ciphertext output file>")
    print("decrypt <ciphertext file> <key file> <plaintext output file>")
    sys.exit(1)


if len(sys.argv) != 5:
    usage()
elif sys.argv[1] == 'encrypt':
    encrypt(sys.argv[2], sys.argv[3], sys.argv[4])
elif sys.argv[1] == 'decrypt':
    decrypt(sys.argv[2], sys.argv[3], sys.argv[4])
else:
    usage()
