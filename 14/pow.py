#!/usr/bin/env python3

import argparse, hashlib, sys, datetime  # do not use any other imports/libraries

# took 3.5 hours (please specify here how much time your solution required)


# parse arguments
parser = argparse.ArgumentParser(description='Proof-of-work solver')
parser.add_argument('--difficulty', default=0, type=int, help='Number of leading zero bits')
args = parser.parse_args()


def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length == False:
        length = (i.bit_length() + 7) // 8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b


difficulty = args.difficulty
# Nonce for 'B87538' is 80924133
# B87538 is my student ID
nonce = 0
start = datetime.datetime.now()
while True:
    input = b'B87538' + nb(nonce, 8)
    solution = hashlib.sha256(hashlib.sha256(input).digest()).hexdigest()
    verify = bin(int('1' + solution, 16))[3:3 + difficulty]
    if difficulty * "0" == verify:
        break
    else:
        nonce += 1

time = datetime.datetime.now() - start
perTime = time.seconds / (nonce / 1000000)
print("[+] Solved in", time.seconds, "sec (" + str(perTime), "Mhash/sec)")
print("[+] Input:", input)
print("[+] Solution:", solution)
print("[+] Nonce:", nonce)
