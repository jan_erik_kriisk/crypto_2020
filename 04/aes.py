#!/usr/bin/python3

import datetime, os, sys
from pyasn1.codec.der import decoder

# $ sudo apt-get install python3-crypto
sys.path = sys.path[1:]  # removes script directory from aes.py search path
from Crypto.Cipher import AES  # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Cipher.AES-module.html
from Crypto.Util.strxor import strxor  # https://www.dlitz.net/software/pycrypto/api/current/Crypto.Util.strxor-module.html#strxor
from hashlib import pbkdf2_hmac
import hashlib, hmac  # do not use any other imports/libraries


# took 10.5 hours (please specify here how much time your solution required)

def bn(b):
    # b - bytes to encode as integer
    # Add the first byte
    i = b[0]
    # Add rest of the bytes
    for j in range(len(b) - 1):
        i = i << 8
        i = i | b[j + 1]
    return i


def nb(i):
    # i - integer to encode as bytes
    # Create bytes object
    b = bytes()
    # Add all the bytes
    while i != 0:
        byte = i & 0xff
        b = bytes([byte]) + b
        i = i >> 8
    return b


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    if len(value_bytes) < 128:
        return bytes([len(value_bytes)])
    else:
        lenBytes = nb(len(value_bytes))
        padding = nb(128 | len(lenBytes))
        return padding + lenBytes


def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    integer = nb(i)
    # If integer is zero add a zero byte
    if i == 0:
        integer = bytes([0x00])
    # If the integers first byte starts with a 1 bit, add a zero byte for padding
    elif integer[0] >> 7 == 1:
        integer = bytes([0x00]) + integer
    return bytes([2]) + asn1_len(integer) + integer


def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([4]) + asn1_len(octets) + octets


def asn1_null():
    # returns DER encoding of NULL
    return bytes([5]) + bytes([0])


def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    # Add the first 2 components
    value = nb(40 * oid[0] + oid[1])
    # Add the rest of the components
    for i in range(len(oid) - 2):
        integer = oid[i + 2]
        # For each component we start from the last byte, for the last byte the first left most bit is 0
        b = bytes()
        byte = (integer & 127)
        b = bytes([byte]) + b
        integer = integer >> 7
        # We encode the rest of the bytes and for each the left most bit will be 1
        while integer != 0:
            byte = (integer & 127)
            byte |= 128
            b = bytes([byte]) + b
            integer = integer >> 7
        value += b
    return bytes([6]) + asn1_len(value) + value


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([48]) + asn1_len(der) + der


# this function benchmarks how many PBKDF2 iterations
# can be performed in one second on the machine it is executed
def benchmark():
    # measure time for performing 10000 iterations
    iter = 0
    totalTime = 0
    start = datetime.datetime.now()
    pbkdf2_hmac('sha1', b'asd', os.urandom(8), 10000, 36)
    stop = datetime.datetime.now()
    time = (stop - start).total_seconds()
    # extrapolate to 1 second
    while totalTime < 1:
        totalTime += time
        iter += 1
    totalTime -= time
    iter -= 1
    iter = int(iter * 10000 + 10000 * (1 - totalTime) / time)
    print("[+] Benchmark: %s PBKDF2 iterations in 1 second" % iter)
    return iter  # returns number of iterations that can be performed in 1 second


def encrypt(pfile, cfile):
    # Benchmarking
    iterations = benchmark()
    # Asking for password
    password = input("[?] Enter password: ").encode()
    # Derieving key
    salt = os.urandom(8)
    originalIV = os.urandom(16)
    iv = originalIV
    key = pbkdf2_hmac('sha1', password, salt, iterations, 36)
    # Get AES key
    key_aes = bytes()
    for i in range(16):
        key_aes += bytes([key[i]])
    # Get Sha1 Key
    key_sha = bytes()
    for i in range(20):
        key_sha += bytes([key[16 + i]])
    # Writing ciphertext in temporary file
    plain = open(pfile, "rb")
    plaintext_block = plain.read(16)
    cipher = AES.new(key_aes)
    cipherText = bytes()
    while plaintext_block:
        if len(plaintext_block) < 16:
            value = 16 - len(plaintext_block)
            plaintext_block += bytes([value]) * value
            cipherText = cipher.encrypt(strxor(plaintext_block, iv)) + cipherText
            break
        else:
            iv = cipher.encrypt(strxor(plaintext_block, iv))
            cipherText = iv + cipherText
            plaintext_block = plain.read(16)
        if len(plaintext_block) == 0:
            cipherText = cipher.encrypt(strxor(bytes([16]) * 16, iv)) + cipherText
            break
    tfile = "tfile"
    tempfile = open(tfile + ".tmp", "wb")
    tempfile.write(cipherText)
    tempfile.close()
    plain.close()
    # Calculating HMAC digest
    hashFile = hmac.new(key_sha, None, hashlib.sha1)
    tempfile = open(tfile + ".tmp", "rb")
    bytesChunk = tempfile.read(512)
    while bytesChunk:
        hashFile.update(bytesChunk)
        bytesChunk = tempfile.read(512)
    digestInfo = hashFile.digest()
    # Writing DER structure in cfile
    asn1 = asn1_sequence(
        asn1_sequence(
            asn1_octetstring(salt) +
            asn1_integer(iterations) +
            asn1_integer(36)
        ) +
        asn1_sequence(
            asn1_objectidentifier([2, 16, 840, 1, 101, 3, 4, 1, 2]) +
            asn1_octetstring(originalIV)
        ) +
        asn1_sequence(
            asn1_sequence(
                asn1_objectidentifier([1, 3, 14, 3, 2, 26]) +
                asn1_null()
            ) +
            asn1_octetstring(digestInfo)
        )
    )
    newFile = open(cfile, "wb")
    newFile.write(asn1)
    # Writing temporary ciphertext file to cfile
    temp = open(tfile + ".tmp", "rb").read()
    newFile.write(temp)
    newFile.close()
    # Deleting temporary ciphertext file
    os.unlink(tfile + ".tmp")
    pass


def decrypt(cfile, pfile):
    # reading DER structure
    cipherfile = open(cfile, "rb")
    decoded = decoder.decode(cipherfile.read())
    cipherfile.seek(0)
    tenBytes = cipherfile.read(10)
    if tenBytes[1] >> 7 == 1:
        lengthBytes = 127 & tenBytes[1]
        length = bytes()
        for i in range(lengthBytes):
            length += bytes([tenBytes[2 + i]])
        offset = 2 + lengthBytes + bn(length)
    else:
        offset = 2 + tenBytes[1]
    # Decoding DER structure
    originalIV = bytes(decoded[0][1][1])
    iteration = decoded[0][0][1]
    salt = bytes(decoded[0][0][0])
    digest = decoded[0][2][1]
    # Asking for password
    password = input("[?] Enter password: ").encode()
    # Derieving key
    key = pbkdf2_hmac('sha1', password, salt, iteration, 36)
    # Get AES key
    key_aes = bytes()
    for i in range(16):
        key_aes += bytes([key[i]])
    # Get Sha1 Key
    key_sha = bytes()
    for i in range(20):
        key_sha += bytes([key[16 + i]])
    # First pass over ciphertext to calculate and verify HMAC
    verifyFile = hmac.new(key_sha, None, hashlib.sha1)
    cipherfile.seek(offset)
    bytesChunk = cipherfile.read(512)
    while bytesChunk:
        verifyFile.update(bytesChunk)
        bytesChunk = cipherfile.read(512)
    digestInfo = verifyFile.digest()
    if digestInfo != digest:
        print("[-] Wrong key or message has been manipulated!")
    else:
        # Second pass over ciphertext to decrypt
        cipherfile.seek(offset)
        ciphertext_block = cipherfile.read(16)
        cipher = AES.new(key_aes)
        cipherText = bytes()
        while ciphertext_block:
            iv = cipherfile.read(16)
            if len(iv) == 0:
                cipherText = strxor(cipher.decrypt(ciphertext_block), originalIV) + cipherText
                break
            else:
                cipherText = strxor(cipher.decrypt(ciphertext_block), iv) + cipherText
                ciphertext_block = iv
        lastByte = cipherText[len(cipherText) - 1]
        cipherText = cipherText[:len(cipherText) - lastByte]
        plainfile = open(pfile, "wb")
        plainfile.write(cipherText)
        plainfile.close()
    pass


def usage():
    print("Usage:")
    print("-encrypt <plaintextfile> <ciphertextfile>")
    print("-decrypt <ciphertextfile> <plaintextfile>")
    sys.exit(1)


if len(sys.argv) != 4:
    usage()
elif sys.argv[1] == '-encrypt':
    encrypt(sys.argv[2], sys.argv[3])
elif sys.argv[1] == '-decrypt':
    decrypt(sys.argv[2], sys.argv[3])
else:
    usage()
