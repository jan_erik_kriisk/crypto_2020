#!/usr/bin/env python3

import argparse, codecs, sys  # do not use any other imports/libraries
from smartcard.CardType import AnyCardType
from smartcard.CardRequest import CardRequest
from smartcard.CardConnection import CardConnection
from smartcard.util import toHexString

# took 1.5 hours (please specify here how much time your solution required)

# parse arguments
parser = argparse.ArgumentParser(description='Fetch certificates from ID card', add_help=False)
parser.add_argument('--cert', type=str, default=None, choices=['auth', 'sign'], help='Which certificate to fetch')
parser.add_argument("--out", required=True, type=str, help="File to store certificate (PEM)")
args = parser.parse_args()

# this will wait for card inserted in any reader
channel = CardRequest(timeout=100, cardType=AnyCardType()).waitforcard().connection
print("[+] Selected reader:", channel.getReader())

# using T=0 for compatibility and simplicity
try:
    channel.connect(CardConnection.T0_protocol)
except:
    # fallback to T=1 if the reader does not support T=0
    channel.connect(CardConnection.T1_protocol)

# detect and print EstEID card type
atr = channel.getATR()
if atr == [0x3B, 0xFE, 0x18, 0x00, 0x00, 0x80, 0x31, 0xFE, 0x45, 0x45, 0x73, 0x74, 0x45, 0x49, 0x44, 0x20, 0x76, 0x65,
           0x72, 0x20, 0x31, 0x2E, 0x30, 0xA8]:
    print("[+] EstEID v3.x on JavaCard")
elif atr == [0x3B, 0xFA, 0x18, 0x00, 0x00, 0x80, 0x31, 0xFE, 0x45, 0xFE, 0x65, 0x49, 0x44, 0x20, 0x2F, 0x20, 0x50, 0x4B,
             0x49, 0x03]:
    print("[+] EstEID v3.5 (10.2014) cold (eID)")
elif atr == [0x3B, 0xDB, 0x96, 0x00, 0x80, 0xB1, 0xFE, 0x45, 0x1F, 0x83, 0x00, 0x12, 0x23, 0x3F, 0x53, 0x65, 0x49, 0x44,
             0x0F, 0x90, 0x00, 0xF1]:
    print("[+] Estonian ID card (2018)")
else:
    print("[-] Unknown card:", toHexString(atr))
    sys.exit(1)


def bn(b):
    # b - bytes to encode as integer
    # Add the first byte
    i = b[0]
    # Add rest of the bytes
    for j in range(len(b) - 1):
        i = i << 8
        i = i | b[j + 1]
    return i


def send(apdu):
    data, sw1, sw2 = channel.transmit(apdu)

    # success
    if [sw1, sw2] == [0x90, 0x00]:
        return data
    # (T=0) card signals how many bytes to read
    elif sw1 == 0x61:
        # print("[=] More data to read:", sw2)
        return send([0x00, 0xC0, 0x00, 0x00, sw2])  # GET RESPONSE of sw2 bytes
    # (T=0) card signals incorrect Le
    elif sw1 == 0x6C:
        # print("[=] Resending with Le:", sw2)
        return send(apdu[0:4] + [sw2])  # resend APDU with Le = sw2
    # probably error condition
    else:
        print("Error: %02x %02x, sending APDU: %s" % (sw1, sw2, toHexString(apdu)))
        sys.exit(1)


# reading from the card auth or sign certificate (EstEID spec page 33)
print("[=] Retrieving %s certificate..." % args.cert)
if args.cert == "auth":
    send([0x00, 0xA4, 0x00, 0x0C])  # SELECT FILE (MF)
    send([0x00, 0xA4, 0x01, 0x0C] + [0x02, 0xEE, 0xEE])  # MF/EEEE
    send([0x00, 0xA4, 0x02, 0x04, 0x02, 0xAA, 0xCE])  # MF/EEEE/AACE
elif args.cert == "sign":
    send([0x00, 0xA4, 0x00, 0x0C])  # SELECT FILE (MF)
    send([0x00, 0xA4, 0x01, 0x0C] + [0x02, 0xEE, 0xEE])  # MF/EEEE
    send([0x00, 0xA4, 0x02, 0x0C, 0x02, 0xDD, 0xCE])  # MF/EEEE/DDCE
else:
    print("[!] Wrong argument!")
    sys.exit()

# read the first 10 bytes to parse ASN.1 length field and determine certificate length
r = send([0x00, 0xB0, 0x00, 0x00, 10])
if r[1] >> 7 == 1:
    lengthBytes = 127 & r[1]
    length = bytes()
    for i in range(lengthBytes):
        length += bytes([r[2 + i]])
    certlen = 2 + lengthBytes + bn(length)
else:
    certlen = 2 + r[1]
print("[+] Certificate size: %d bytes" % certlen)

# reading DER encoded certificate from smart card
cert = bytes()
chunk = bytes()
for byte in r:
    chunk += bytes([byte])
cert += chunk
i = 10
while True:
    offset = [i >> 8, i & 0xFF]
    r = send([0x00, 0xB0, offset[0], offset[1], 231])
    chunk = bytes()
    if i + 231 > certlen:
        for j in range(certlen - i):
            chunk += bytes([r[j]])
        cert += chunk
        break
    else:
        for byte in r:
            chunk += bytes([byte])
        cert += chunk
        i += 231

# save certificate in PEM form
open(args.out, "wb").write(
    b"-----BEGIN CERTIFICATE-----\n" + codecs.encode(cert, 'base64') + b"-----END CERTIFICATE-----\n")
print("[+] Certificate stored in", args.out)
