#!/usr/bin/python3

# do not use any other imports/libraries
import codecs
import hashlib
import datetime
import sys
import zipfile

# apt-get install python3-bs4 python3-pyasn1-modules python3-ecdsa python3-m2crypto python3-lxml
from ecdsa import VerifyingKey
from bs4 import BeautifulSoup
from pyasn1.codec.der import decoder, encoder
from pyasn1_modules import rfc2560


# took 5.0 hours (please specify here how much time your solution required)

# verifies signature using ECDSA with SHA384
def verify_ecdsa(cert, signature_value, signed_msg):
    import M2Crypto
    X509 = M2Crypto.X509.load_cert_der_string(cert)
    pk = X509.get_pubkey()
    pubkey = pk.as_der()  # public key info structure

    # ecdsa-sha384:
    vk = VerifyingKey.from_der(pubkey)
    return vk.verify(signature_value, signed_msg, hashfunc=hashlib.sha384)


# extracts from TSA response the timestamp and timestamped DigestInfo
def parseTsaResponse(timestamp_resp):
    timestamp = decoder.decode(timestamp_resp)
    tsinfo = decoder.decode(timestamp[0][1][2][1])[0]
    ts_digestinfo = encoder.encode(tsinfo[2])
    ts = datetime.datetime.strptime(str(tsinfo[4]), '%Y%m%d%H%M%SZ')
    # let's assume that timestamp has been issued by a trusted TSA
    return ts, ts_digestinfo


# extracts from OCSP response certID_serial, certStatus and thisUpdate
def parseOcspResponse(ocsp_resp):
    ocspResponse, _ = decoder.decode(ocsp_resp, asn1Spec=rfc2560.OCSPResponse())
    responseStatus = ocspResponse.getComponentByName('responseStatus')
    assert responseStatus == rfc2560.OCSPResponseStatus('successful'), responseStatus.prettyPrint()
    responseBytes = ocspResponse.getComponentByName('responseBytes')
    responseType = responseBytes.getComponentByName('responseType')
    assert responseType == rfc2560.id_pkix_ocsp_basic, responseType.prettyPrint()
    response = responseBytes.getComponentByName('response')
    basicOCSPResponse, _ = decoder.decode(response, asn1Spec=rfc2560.BasicOCSPResponse())
    tbsResponseData = basicOCSPResponse.getComponentByName('tbsResponseData')
    response0 = tbsResponseData.getComponentByName('responses').getComponentByPosition(0)
    # let's assume that OCSP response has been signed by a trusted OCSP responder
    certID = response0.getComponentByName('certID')
    # let's assume that issuer name and key hashes in certID are correct
    certID_serial = certID[3]
    certStatus = response0.getComponentByName('certStatus').getName()
    thisUpdate = datetime.datetime.strptime(str(response0.getComponentByName('thisUpdate')), '%Y%m%d%H%M%SZ')

    return certID_serial, certStatus, thisUpdate


# returns XML canonicalization of element with specified tagname
def canonicalize(full_xml, tagname):
    if type(full_xml) != bytes:
        print("[-] canonicalize(): input not xml:", type(full_xml))
        exit(1)
    import io
    import lxml.etree as ET
    input = io.BytesIO(full_xml)
    et = ET.parse(input)
    output = io.BytesIO()
    ET.ElementTree(et.find('.//{*}' + tagname)).write_c14n(output)
    return output.getvalue()


# returns CommonName value from certificate's Subject Distinguished Name field
def get_subject_cn(cert_der):
    # looping over Distinguished Name entries until CN found
    subject_dn = decoder.decode(cert_der)[0][0][5]
    set = None
    for i in range(len(subject_dn)):
        if subject_dn[i][0][0] == (2, 5, 4, 3):
            set = i
            break
    return subject_dn[set][0][1]


filename = sys.argv[1]

# get and decode xml
archive = zipfile.ZipFile(filename, 'r')
xml = archive.read('META-INF/signatures0.xml')

# let's trust this certificate
xmldoc = BeautifulSoup(xml, features="xml")
signers_cert_der = codecs.decode(xmldoc.XAdESSignatures.KeyInfo.X509Data.X509Certificate.encode_contents(), 'base64')
file_name = xmldoc.XAdESSignatures.Signature.SignedInfo.Reference['URI']
print("[+] Signatory:", get_subject_cn(signers_cert_der))
print("[+] Signed file:", file_name)

# perform all kinds of checks
# get the responses from xml
tsa_response = codecs.decode(xmldoc.XAdESSignatures.Object.QualifyingProperties.UnsignedProperties.
                             UnsignedSignatureProperties.SignatureTimeStamp.EncapsulatedTimeStamp.encode_contents(),
                             'base64')
ocsp_response = codecs.decode(xmldoc.XAdESSignatures.Object.QualifyingProperties.UnsignedProperties.
                              UnsignedSignatureProperties.RevocationValues.OCSPValues.EncapsulatedOCSPValue.
                              encode_contents(), 'base64')

# parse the responses
parsed_tsa = parseTsaResponse(tsa_response)
parsed_ocsp = parseOcspResponse(ocsp_response)

# print the timestamp
timestamp = parsed_tsa[0]
print("[+] Timestamped:", timestamp)

# check if the certificate is valid or not
cert_status = parsed_ocsp[1]
if cert_status != "good":
    print("[-] Certificate is " + cert_status + "!")
    sys.exit()

# validate the certificate serial number
certID_serial = str(parsed_ocsp[0])
signed_certID_serial = xmldoc.XAdESSignatures.Object.QualifyingProperties.SignedProperties.SignedSignatureProperties. \
    SigningCertificate.Cert.IssuerSerial.X509SerialNumber.encode_contents().decode()
if certID_serial != signed_certID_serial:
    print("[-] Wrong certificate ID serial!")
    sys.exit()

# check if the TSA and OCSP times correlate (OCSP timestamp is +1 second compared to TSA)
this_update = parsed_ocsp[2]
if timestamp != this_update - datetime.timedelta(seconds=1):
    print("[-] Timestamp of TSA doesn't correlate with OCSP's timestamp!")
    sys.exit()

# validate the signature properties hash
signed_props = hashlib.sha256(canonicalize(xml, "SignedProperties")).digest()
signed_hash = codecs.decode(xmldoc.XAdESSignatures.Signature.SignedInfo.
                            find('Reference', attrs={'URI': '#S0-SignedProperties'}).
                            DigestValue.encode_contents(), 'base64')
if signed_props != signed_hash:
    print("[-] Wrong certificate hash included under the signed info!")
    sys.exit()

# validate the files signed info
file = hashlib.sha256(archive.read('hello.txt')).digest()
file_hash = codecs.decode(xmldoc.XAdESSignatures.Signature.SignedInfo.find('Reference', attrs={'Id': 'S0-RefId0'}).
                          DigestValue.encode_contents(), 'base64')
if file != file_hash:
    print("[-] Wrong certificate hash included under the file signed info!")
    sys.exit()

# finally verify signatory's signature
signature_value = codecs.decode(xmldoc.XAdESSignatures.Signature.SignatureValue.encode_contents(), 'base64')
signed_info_str = canonicalize(xml, "SignedInfo")
if verify_ecdsa(signers_cert_der, signature_value, signed_info_str):
    print("[+] Signature verification successful!")
