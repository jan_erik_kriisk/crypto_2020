#!/usr/bin/python3

import codecs, hashlib, sys

from pyasn1.codec.der import decoder

sys.path = sys.path[1:]  # don't remove! otherwise the library import below will try to import your hmac.py
import hmac  # do not use any other imports/libraries


# took 3.5 hours (please specify here how much time your solution required)

def nb(i):
    # i - integer to encode as bytes
    # Create bytes object
    b = bytes()
    # Add all the bytes
    while i != 0:
        byte = i & 0xff
        b = bytes([byte]) + b
        i = i >> 8
    return b


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    if len(value_bytes) < 128:
        return bytes([len(value_bytes)])
    else:
        lenBytes = nb(len(value_bytes))
        padding = nb(128 | len(lenBytes))
        return padding + lenBytes


def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([4]) + asn1_len(octets) + octets


def asn1_null():
    # returns DER encoding of NULL
    return bytes([5]) + bytes([0])


def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    # Add the first 2 components
    value = nb(40 * oid[0] + oid[1])
    # Add the rest of the components
    for i in range(len(oid) - 2):
        integer = oid[i + 2]
        # For each component we start from the last byte, for the last byte the first left most bit is 0
        b = bytes()
        byte = (integer & 127)
        b = bytes([byte]) + b
        integer = integer >> 7
        # We encode the rest of the bytes and for each the left most bit will be 1
        while integer != 0:
            byte = (integer & 127)
            byte |= 128
            b = bytes([byte]) + b
            integer = integer >> 7
        value += b
    return bytes([6]) + asn1_len(value) + value


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([48]) + asn1_len(der) + der


def verify(filename):
    print("[+] Reading HMAC DigestInfo from", filename + ".hmac")
    # Open and read the file
    file = open(filename + ".hmac", "rb").read()
    # Decode the file
    decoded = decoder.decode(file)
    # Find the OID and the DigestInfo
    algorithm = decoded[0][0][0]
    digest = bytes(decoded[0][1])
    # Find out what kind of algorithm do we have to use
    coding = hashlib
    codingName = ""
    if algorithm == (1, 2, 840, 113549, 2, 5):
        coding = hashlib.md5
        codingName = "MD5"
    elif algorithm == (1, 3, 14, 3, 2, 26):
        coding = hashlib.sha1
        codingName = "SHA1"
    elif algorithm == (2, 16, 840, 1, 101, 3, 4, 2, 1):
        coding = hashlib.sha3_256
        codingName = "SHA256"
    print("[+] HMAC-" + codingName + " digest: " + codecs.encode(digest, "hex").decode())
    # Enter the key
    key = input("[?] Enter key: ").encode()
    # Create the hash
    hashFile = hmac.new(key, None, coding)
    # Do the calculation from the original file
    file = open(filename, "rb")
    # Read the file 512 bytes at a time and update the hash with the info
    bytesChunk = file.read(512)
    while bytesChunk:
        hashFile.update(bytesChunk)
        bytesChunk = file.read(512)
    digest_calculated = bytes(hashFile.digest())
    print("[+] Calculated HMAC-" + codingName + ": " + codecs.encode(digest_calculated, "hex").decode())
    # Compare the 2 values
    if digest_calculated != digest:
        print("[-] Wrong key or message has been manipulated!")
    else:
        print("[+] HMAC verification successful!")


def mac(filename):
    # Enter the key
    key = input("[?] Enter key: ").encode()
    # Create the hash
    hashFile = hmac.new(key, None, hashlib.sha3_256)
    # Open the file
    file = open(filename, "rb")
    # Read the file 512 bytes at a time and update the hash with the info
    bytesChunk = file.read(512)
    while bytesChunk:
        hashFile.update(bytesChunk)
        bytesChunk = file.read(512)
    digestInfo = hashFile.digest()
    # Encode the DigestInfo in ASN1
    asn1 = asn1_sequence(
        asn1_sequence(
            asn1_objectidentifier([2, 16, 840, 1, 101, 3, 4, 2, 1]) +
            asn1_null()
        ) +
        asn1_octetstring(digestInfo)
    )
    # Write the ASN1 info into a .hmac file
    newFile = open(filename + ".hmac", "wb")
    newFile.write(asn1)
    newFile.close()
    print("[+] Calculated HMAC-SHA256: " + codecs.encode(bytes(digestInfo), "hex").decode())
    print("[+] Writing HMAC DigestInfo to", filename + ".hmac")


def usage():
    print("Usage:")
    print("-verify <filename>")
    print("-mac <filename>")
    sys.exit(1)


if len(sys.argv) != 3:
    usage()
elif sys.argv[1] == '-mac':
    mac(sys.argv[2])
elif sys.argv[1] == '-verify':
    verify(sys.argv[2])
else:
    usage()
