package appcrypto;

import javacard.framework.*;
import javacard.security.*;
import javacardx.crypto.*;

// took 5.5 hours (please specify here how much time your solution required)

public class TestApplet extends Applet {
	
	private static KeyPair keypair;
	private static RSAPublicKey pub;
	private static Cipher rsa;
	private static boolean generated;
	
	public static void install(byte[] ba, short offset, byte len) {
		(new TestApplet()).register();
		keypair = new KeyPair(KeyPair.ALG_RSA, KeyBuilder.LENGTH_RSA_2048);
		pub = (RSAPublicKey) keypair.getPublic();
		rsa = Cipher.getInstance(Cipher.ALG_RSA_PKCS1, false);
		generated = false;
	}
	
	public void process(APDU apdu) {
		byte[] buf = apdu.getBuffer();
		switch (buf[ISO7816.OFFSET_INS]) {
		case (0x02):
			if (generated == false) {
				keypair.genKeyPair();
				generated = true;
			}
			return;
		case (0x04):
			short e_offset = 0;
			short e_len = pub.getExponent(buf, e_offset);
			apdu.setOutgoingAndSend(e_offset, e_len);
			return;
		case (0x06):
			short m_offset = 0;
			short m_len = pub.getModulus(buf, m_offset);
			apdu.setOutgoingAndSend(m_offset, m_len);
			return;
		case (0x08):
			apdu.setIncomingAndReceive(); 
			rsa.init(keypair.getPrivate(), Cipher.MODE_DECRYPT);
			Util.arrayCopyNonAtomic(buf, (short) 2, buf, (short) 3, (short) 2);
			short len = rsa.doFinal(buf, (short) 3, (short) 256, buf, (short) 0);
			apdu.setOutgoingAndSend((short) 0, len);
			return;
		}
		ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);		
	}
}