#!/usr/bin/env python3

# sudo apt install python3-socks
import argparse
import socks
import socket
import sys
import random

# do not use any other imports/libraries

# took 5.5 hours (please specify here how much time your solution required)


# parse arguments
parser = argparse.ArgumentParser(description='TorChat client')
parser.add_argument('--myself', required=True, type=str, help='My TorChat ID')
parser.add_argument('--peer', required=True, type=str, help='Peer\'s TorChat ID')
args = parser.parse_args()

# route outgoing connections through Tor
socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 9050)
start = socks.socksocket()


# reads and returns torchat command from the socket
def read_torchat_cmd(incoming_socket):
    # read until newline
    cmd = b''
    byte = incoming_socket.recv(1)
    while byte != b'\n':
        cmd += byte
        byte = incoming_socket.recv(1)
    # return command
    return cmd.decode()


# prints torchat command and sends it
def send_torchat_cmd(outgoing_socket):
    command = 'add_me'
    print('[+] Sending:', command)
    outgoing_socket.send((command + "\n").encode())
    command = 'status available'
    print('[+] Sending:', command)
    outgoing_socket.send((command + "\n").encode())
    command = 'profile_name Godfather'
    print('[+] Sending:', command)
    outgoing_socket.send((command + "\n").encode())
    pass


# connecting to peer
print('[+] Connecting to peer', args.peer)
peerAddr = args.peer + '.onion'
myself = args.myself
start.connect((peerAddr, 11009))
# sending ping
mycookie = str(random.getrandbits(128))
message = "ping " + myself + " " + mycookie
print('[+] Sending:', message)
start.send((message + "\n").encode())

# listening for the incoming connection
print("[+] Listening...")
sserv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sserv.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
sserv.bind(('', 8888))
sserv.listen(0)
(s, address) = sserv.accept()
incoming_socket = s
print("[+] Client %s:%s" % (address[0], address[1]))

incoming_authenticated = False
status_received = False
info_sent = False
cookie_peer = ""
# main loop for processing the received commands
while True:
    cmdr = read_torchat_cmd(incoming_socket)
    cmd = cmdr.split(' ')
    type = cmd[0]
    if type == 'ping':
        if args.peer == cmd[1]:
            cookie_peer = cmd[2]
            message = ''
            for i in cmd:
                message += i + " "
            print('[+] Received:', message)
        else:
            print('[-] Incorrect Peer ID:', cmd[1])
            sys.exit()
    elif type == 'pong':
        message = ''
        for i in cmd:
            message += i + " "
        print('[+] Received:', message)
        if mycookie == cmd[1]:
            print('[+] Incoming connection authenticated!')
            incoming_authenticated = True
            message = "pong " + cookie_peer
            print('[+] Sending:', message)
            start.send((message + "\n").encode())
    elif type == 'client' or type == 'version' or type == 'status' or type == 'profile_name':
        message = ''
        for i in cmd:
            message += i + " "
        print('[+] Received:', message)
        if type == 'status':
            status_received = True
            if not info_sent:
                send_torchat_cmd(start)
                info_sent = True
    elif status_received and incoming_authenticated and type == 'message':
        message = ''
        for i in cmd:
            message += i + " "
        print('[+] Received:', message)
        message = 'message ' + input('[?] Enter message: ')
        print('[+] Sending:', message)
        start.send((message + '\n').encode())
