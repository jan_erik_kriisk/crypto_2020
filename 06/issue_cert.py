#!/usr/bin/env python3

import argparse, codecs, hashlib, os, sys  # do not use any other imports/libraries
from pyasn1.codec.der import decoder, encoder

# took 9.0 hours (please specify here how much time your solution required)


# parse arguments
parser = argparse.ArgumentParser(description='issue TLS server certificate based on CSR', add_help=False)
parser.add_argument("private_key_file", help="Private key file (in PEM or DER form)")
parser.add_argument("CA_cert_file", help="CA certificate (in PEM or DER form)")
parser.add_argument("csr_file", help="CSR file (in PEM or DER form)")
parser.add_argument("output_cert_file", help="File to store certificate (in PEM form)")
args = parser.parse_args()


def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length == False:
        length = (i.bit_length() + 7) // 8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b


def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i


# ==== ASN1 encoder start ====


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    if len(value_bytes) < 128:
        return bytes([len(value_bytes)])
    else:
        lenBytes = nb(len(value_bytes))
        padding = nb(128 | len(lenBytes))
        return padding + lenBytes


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'
    return bytes([0x01]) + asn1_len(bool) + bool


def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    integer = nb(i)
    # If integer is zero add a zero byte
    if i == 0:
        integer = bytes([0x00])
    # If the integers first byte starts with a 1 bit, add a zero byte for padding
    elif integer[0] >> 7 == 1:
        integer = bytes([0x00]) + integer
    return bytes([2]) + asn1_len(integer) + integer


def asn1_bitstring(bitstr):
    # bitstr - string containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    padding = 0
    # Add zeroes until bitstr is 1 byte
    while len(bitstr) % 8 != 0:
        bitstr += "0"
        padding += 1
    i = 0
    # Flag is added to know when the padding ends and system function needs to be changed
    flag = True
    # If we get 8 zero bits in the padding than we mark it as 1 zero byte
    zeros = 0
    zeroBytes = 0
    # Convert bitstr to integer
    for bit in bitstr:
        i <<= 1
        # If the padding ends we stop counting the zero bits
        if bit == "1":
            i |= 1
            flag = False
        elif flag:
            zeros += 1
            if zeros == 8:
                zeroBytes += 1
                zeros = 0
    # We put all of the necessary padding zero bytes into the value byte
    paddingStr = bytes([padding]) + b'\x00' * zeroBytes + nb(i)
    return bytes([3]) + asn1_len(paddingStr) + paddingStr


def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([4]) + asn1_len(octets) + octets


def asn1_null():
    # returns DER encoding of NULL
    return bytes([5]) + bytes([0])


def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    # Add the first 2 components
    value = nb(40 * oid[0] + oid[1])
    # Add the rest of the components
    for i in range(len(oid) - 2):
        integer = oid[i + 2]
        # For each component we start from the last byte, for the last byte the first left most bit is 0
        b = bytes()
        byte = (integer & 127)
        b = bytes([byte]) + b
        integer = integer >> 7
        # We encode the rest of the bytes and for each the left most bit will be 1
        while integer != 0:
            byte = (integer & 127)
            byte |= 128
            b = bytes([byte]) + b
            integer = integer >> 7
        value += b
    return bytes([6]) + asn1_len(value) + value


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([48]) + asn1_len(der) + der


def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    return bytes([49]) + asn1_len(der) + der


def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    return bytes([19]) + asn1_len(string) + string


def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    return bytes([23]) + asn1_len(time) + time


def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    if tag <= 30:
        tag = tag & 0x1f
    return bytes([160 | tag]) + asn1_len(der) + der


def asn1_utf8string(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of utf8string
    return bytes([12]) + asn1_len(string) + string


def asn1_bitstring_der(bytestr):
    paddingstr = b'\x00' + bytestr
    return bytes([3]) + asn1_len(paddingstr) + paddingstr


# ==== ASN1 encoder end ====


def pem_to_der(content):
    # converts PEM content (if it is PEM) to DER
    if content[0] == b'-----BEGIN CERTIFICATE REQUEST-----\n' and \
            content[-1] == b'-----END CERTIFICATE REQUEST-----\n' or \
            content[0] == b'-----BEGIN CERTIFICATE-----\n' and \
            content[-1] == b'-----END CERTIFICATE-----\n':
        content = content[1:-1]
    text = bytes()
    for line in content:
        text += line
    return codecs.decode(text, 'base64')


def get_privkey(filename):
    # Reads private key file and returns (n, d)
    content = open(filename, "rb").readlines()
    if content[0] == b'-----BEGIN RSA PRIVATE KEY-----\n' and \
            content[-1] == b'-----END RSA PRIVATE KEY-----\n':
        content = pem_to_der(content[1:-1])
    else:
        text = bytes()
        for line in content:
            text += line
        content = text
    # Decode the file
    privkey = decoder.decode(content)
    return int(privkey[0][1]), int(privkey[0][3])


def pkcsv15pad_sign(plaintext, n):
    # pad plaintext for signing according to PKCS#1 v1.5
    # calculate byte length of modulus n
    nr = len(nb(n)) - (3 + len(plaintext))
    # plaintext must be at least 3 bytes smaller than the modulus
    if len(plaintext) >= n - 3:
        print("[-] Plaintext is too large")
        pass
    # generate padding bytes
    padded_plaintext = b'\x00' + b'\x01' + b'\xFF' * nr + b'\x00' + plaintext
    return padded_plaintext


def digestinfo_der(m):
    # returns ASN.1 DER encoded DigestInfo structure containing SHA256 digest of m
    digestInfo = hashlib.sha256(m).digest()
    asn1 = asn1_sequence(
        asn1_sequence(
            asn1_objectidentifier([1, 2, 840, 113549, 1, 1, 11]) +
            asn1_null()
        ) +
        asn1_octetstring(digestInfo)
    )
    return asn1


def sign(m, keyfile):
    # sign DigestInfo of message m
    der = digestinfo_der(m)
    privkey = get_privkey(keyfile)
    n = privkey[0]
    d = privkey[1]
    padded_plaintext = pkcsv15pad_sign(der, n)
    m = bn(padded_plaintext)
    # Warning: make sure that signaturefile produced has the same
    # length as the modulus (hint: use parametrized nb()).
    signature = nb(pow(m, d, n), len(nb(n)))
    return signature


def get_subject_con(csr_der):
    sub_der = decoder.decode(csr_der)[0][0][1]
    set = None
    for i in range(len(sub_der)):
        if sub_der[i][0][0] == (2, 5, 4, 6):
            set = i
            break
    return sub_der[set][0][1]


def get_subject_on(csr_der):
    sub_der = decoder.decode(csr_der)[0][0][1]
    set = None
    for i in range(len(sub_der)):
        if sub_der[i][0][0] == (2, 5, 4, 10):
            set = i
            break
    return sub_der[set][0][1]


def get_subject_oun(csr_der):
    sub_der = decoder.decode(csr_der)[0][0][1]
    set = None
    for i in range(len(sub_der)):
        if sub_der[i][0][0] == (2, 5, 4, 11):
            set = i
            break
    return sub_der[set][0][1]


def get_subject_cn(csr_der):
    # return CommonName value from CSR's Distinguished Name
    # looping over Distinguished Name entries until CN found
    sub_der = decoder.decode(csr_der)[0][0][1]
    set = None
    for i in range(len(sub_der)):
        if sub_der[i][0][0] == (2, 5, 4, 3):
            set = i
            break
    return sub_der[set][0][1]


def get_subjectPublicKeyInfo(csr_der):
    # returns DER encoded subjectPublicKeyInfo from CSR
    return encoder.encode(decoder.decode(csr_der)[0][0][2])


def get_subjectName(cert_der):
    # return subject name DER from CA certificate
    return encoder.encode(decoder.decode(cert_der)[0][0][5])


def issue_certificate(private_key_file, issuer, subject, pubkey):
    # receives:
    # - CA private key filename,
    # - DER encoded CA Distinguished Name,
    # - constructed DER encoded subject's Distinguished Name,
    # - DER encoded subjectPublicKeyInfo
    # returns X.509v3 certificate in PEM format
    data = asn1_sequence(
        asn1_tag_explicit(asn1_integer(2), 0) +
        asn1_integer(908108597) +
        asn1_sequence(
            asn1_objectidentifier([1, 2, 840, 113549, 1, 1, 11]) +
            asn1_null()
        ) +
        issuer +
        asn1_sequence(
            asn1_utctime(b'200120000000Z') +
            asn1_utctime(b'200620000000Z')
        ) +
        subject +
        pubkey +
        asn1_tag_explicit(asn1_sequence(
            # basicConstraints
            asn1_sequence(
                asn1_objectidentifier([2, 5, 29, 19]) +
                asn1_boolean(True) +
                asn1_octetstring(
                    asn1_sequence(
                        asn1_boolean(False)
                    )
                )
            ) +
            # keyUsage
            asn1_sequence(
                asn1_objectidentifier([2, 5, 29, 15]) +
                asn1_boolean(True) +
                asn1_octetstring(
                    asn1_bitstring('100000000')
                )
            ) +
            # extKeyUsageSyntax
            asn1_sequence(
                asn1_objectidentifier([2, 5, 29, 37]) +
                asn1_boolean(True) +
                asn1_octetstring(
                    asn1_sequence(
                        asn1_objectidentifier([1, 3, 6, 1, 5, 5, 7, 3, 1])
                    )
                )
            )
        ), 3)
    )
    signature = sign(data, private_key_file)
    der = asn1_sequence(
        data +
        asn1_sequence(
            asn1_objectidentifier([1, 2, 840, 113549, 1, 1, 11]) +
            asn1_null()
        ) +
        asn1_bitstring_der(signature)
    )
    pem = codecs.encode(der, 'base64')
    return pem


# obtain subject's CN from CSR
csr_der = pem_to_der(open(args.csr_file, 'rb').readlines())
subject_con_text = get_subject_con(csr_der)
subject_on_text = get_subject_on(csr_der)
subject_oun_text = get_subject_oun(csr_der)
subject_cn_text = get_subject_cn(csr_der)

print("[+] Issuing certificate for \"%s\"" % (subject_cn_text))

# obtain subjectPublicKeyInfo from CSR
pubkey = get_subjectPublicKeyInfo(csr_der)

# construct subject name DN
subject = asn1_sequence(
    asn1_set(
        asn1_sequence(
            # commonName
            asn1_objectidentifier([2, 5, 4, 3]) +
            asn1_utf8string(bytes(subject_cn_text))
        )
    )
)
# get subject name DN from CA certificate
CAcert = pem_to_der(open(args.CA_cert_file, 'rb').readlines())
CAsubject = get_subjectName(CAcert)

# issue certificate
cert_pem = issue_certificate(args.private_key_file, CAsubject, subject, pubkey)
file = open(args.output_cert_file, 'wb')
file.write(b'-----BEGIN CERTIFICATE-----\n')
file.write(cert_pem)
file.write(b'-----END CERTIFICATE-----\n')
