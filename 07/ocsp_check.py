#!/usr/bin/env python3

import codecs, datetime, hashlib, re, sys, socket  # do not use any other imports/libraries
from urllib.parse import urlparse
from pyasn1.codec.der import decoder, encoder
from pyasn1.type import namedtype, univ

# sudo apt install python3-pyasn1-modules
from pyasn1_modules import rfc2560, rfc5280


# took 11.0 hours (please specify here how much time your solution required)

def nb(i, length=False):
    # converts integer to bytes
    b = b''
    if length == False:
        length = (i.bit_length() + 7) // 8
    for _ in range(length):
        b = bytes([i & 0xff]) + b
        i >>= 8
    return b


def bn(b):
    # converts bytes to integer
    i = 0
    for char in b:
        i <<= 8
        i |= char
    return i


# ==== ASN1 encoder start ====


def asn1_len(value_bytes):
    # helper function - should be used in other functions to calculate length octet(s)
    # value_bytes - bytes containing TLV value byte(s)
    # returns length (L) byte(s) for TLV
    if len(value_bytes) < 128:
        return bytes([len(value_bytes)])
    else:
        lenBytes = nb(len(value_bytes))
        padding = nb(128 | len(lenBytes))
        return padding + lenBytes


def asn1_boolean(bool):
    # BOOLEAN encoder has been implemented for you
    if bool:
        bool = b'\xff'
    else:
        bool = b'\x00'
    return bytes([0x01]) + asn1_len(bool) + bool


def asn1_integer(i):
    # i - arbitrary integer (of type 'int' or 'long')
    # returns DER encoding of INTEGER
    integer = nb(i)
    # If integer is zero add a zero byte
    if i == 0:
        integer = bytes([0x00])
    # If the integers first byte starts with a 1 bit, add a zero byte for padding
    elif integer[0] >> 7 == 1:
        integer = bytes([0x00]) + integer
    return bytes([2]) + asn1_len(integer) + integer


def asn1_bitstring(bitstr):
    # bitstr - string containing bitstring (e.g., "10101")
    # returns DER encoding of BITSTRING
    padding = 0
    # Add zeroes until bitstr is 1 byte
    while len(bitstr) % 8 != 0:
        bitstr += "0"
        padding += 1
    i = 0
    # Flag is added to know when the padding ends and system function needs to be changed
    flag = True
    # If we get 8 zero bits in the padding than we mark it as 1 zero byte
    zeros = 0
    zeroBytes = 0
    # Convert bitstr to integer
    for bit in bitstr:
        i <<= 1
        # If the padding ends we stop counting the zero bits
        if bit == "1":
            i |= 1
            flag = False
        elif flag:
            zeros += 1
            if zeros == 8:
                zeroBytes += 1
                zeros = 0
    # We put all of the necessary padding zero bytes into the value byte
    paddingStr = bytes([padding]) + b'\x00' * zeroBytes + nb(i)
    return bytes([3]) + asn1_len(paddingStr) + paddingStr


def asn1_octetstring(octets):
    # octets - arbitrary byte string (e.g., b"abc\x01")
    # returns DER encoding of OCTETSTRING
    return bytes([4]) + asn1_len(octets) + octets


def asn1_null():
    # returns DER encoding of NULL
    return bytes([5]) + bytes([0])


def asn1_objectidentifier(oid):
    # oid - list of integers representing OID (e.g., [1,2,840,123123])
    # returns DER encoding of OBJECTIDENTIFIER
    # Add the first 2 components
    value = nb(40 * oid[0] + oid[1])
    # Add the rest of the components
    for i in range(len(oid) - 2):
        integer = oid[i + 2]
        # For each component we start from the last byte, for the last byte the first left most bit is 0
        b = bytes()
        byte = (integer & 127)
        b = bytes([byte]) + b
        integer = integer >> 7
        # We encode the rest of the bytes and for each the left most bit will be 1
        while integer != 0:
            byte = (integer & 127)
            byte |= 128
            b = bytes([byte]) + b
            integer = integer >> 7
        value += b
    return bytes([6]) + asn1_len(value) + value


def asn1_sequence(der):
    # der - DER bytes to encapsulate into sequence
    # returns DER encoding of SEQUENCE
    return bytes([48]) + asn1_len(der) + der


def asn1_set(der):
    # der - DER bytes to encapsulate into set
    # returns DER encoding of SET
    return bytes([49]) + asn1_len(der) + der


def asn1_printablestring(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of PrintableString
    return bytes([19]) + asn1_len(string) + string


def asn1_utctime(time):
    # time - bytes containing timestamp in UTCTime format (e.g., b"121229010100Z")
    # returns DER encoding of UTCTime
    return bytes([23]) + asn1_len(time) + time


def asn1_tag_explicit(der, tag):
    # der - DER encoded bytestring
    # tag - tag value to specify in the type octet
    # returns DER encoding of original DER that is encapsulated in tag type
    if tag <= 30:
        tag = tag & 0x1f
    return bytes([160 | tag]) + asn1_len(der) + der


def asn1_utf8string(string):
    # string - bytes containing printable characters (e.g., b"foo")
    # returns DER encoding of utf8string
    return bytes([12]) + asn1_len(string) + string


def asn1_bitstring_der(bytestr):
    paddingstr = b'\x00' + bytestr
    return bytes([3]) + asn1_len(paddingstr) + paddingstr


# ==== ASN1 encoder end ====


def pem_to_der(content):
    # converts PEM encoded X.509 certificate (if it is PEM) to DER
    if content[0] == b'-----BEGIN CERTIFICATE-----\n' and \
            content[-1] == b'-----END CERTIFICATE-----\n':
        content = content[1:-1]
    text = bytes()
    for line in content:
        text += line
    return codecs.decode(text, 'base64')


def get_name(cert):
    return encoder.encode(decoder.decode(cert))


def get_key(cert):
    # get subjectPublicKey from certificate
    return encoder.encode(decoder.decode(cert))


def get_serial(cert):
    # get serial from certificate
    return encoder.encode(decoder.decode(cert))


def produce_request(cert, issuer_cert):
    name = get_name(issuer_cert)
    key = get_key(issuer_cert)
    serial = get_serial(cert)
    # make OCSP request in ASN.1 DER form
    # construct CertID (use SHA1)
    print("[+] Querying OCSP for serial:", serial)
    request = asn1_sequence(
        asn1_sequence(
            asn1_tag_explicit(asn1_integer(2), 0) +
            asn1_sequence(
                asn1_sequence(
                    asn1_sequence(
                        asn1_objectidentifier([1, 3, 14, 3, 2, 26]) +
                        asn1_null()
                    ) +
                    asn1_octetstring(hashlib.sha1(name).digest()) +
                    asn1_octetstring(hashlib.sha1(key).digest()) +
                    asn1_integer(serial)
                )
            )
        )
    )
    return request


def send_req(ocsp_req, ocsp_url):
    # send OCSP request to OCSP responder
    # parse ocsp responder url
    host = urlparse(ocsp_url).netloc
    print("[+] Connecting to %s..." % (host))
    # parse ocsp responder url
    getURL = host + urlparse(issuer_cert_url).path
    # connect to host
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, 80))
    # sent HTTP GET request
    s.send(b'POST / HTTP/1.1\r\nHost: ' + getURL.encode() + b'\r\nContent-Length: ' + bytes([len(ocsp_req)]) +
           b'\r\n\r\n' + ocsp_req)
    # read HTTP response header
    header = bytes()
    while header[-4:] != b'\r\n\r\n':
        header += s.recv(1)
    # get HTTP response length
    bodyLength = re.search('content-length:\s*(\d+)\s', header.decode(), re.S + re.I).group(1)
    # read HTTP response body
    ocsp_resp = bytes()
    for i in range(int(bodyLength)):
        ocsp_resp += s.recv(1)
    return ocsp_resp


def get_ocsp_url(cert):
    # get OCSP url from certificate's AIA extension
    # pyasn1 syntax description to decode AIA extension
    class AccessDescription(univ.Sequence):
        componentType = namedtype.NamedTypes(
            namedtype.NamedType('accessMethod', univ.ObjectIdentifier()),
            namedtype.NamedType('accessLocation', rfc5280.GeneralName()))

    class AuthorityInfoAccessSyntax(univ.SequenceOf):
        componentType = AccessDescription()

    # looping over certificate extensions
    for seq in decoder.decode(cert)[0][0][7]:
        if str(seq[0]) == '1.3.6.1.5.5.7.1.1':  # look for AIA extension
            ext_value = bytes(seq[1])
            for aia in decoder.decode(ext_value, asn1Spec=AuthorityInfoAccessSyntax())[0]:
                if str(aia[0]) == '1.3.6.1.5.5.7.48.1':  # ocsp url
                    return str(aia[1].getComponentByName('uniformResourceIdentifier'))
    print("[-] OCSP url not found in certificate!")
    exit(1)


def get_issuer_cert_url(cert):
    # get CA certificate url from certificate's AIA extension (hint: see get_ocsp_url())
    # pyasn1 syntax description to decode AIA extension
    class AccessDescription(univ.Sequence):
        componentType = namedtype.NamedTypes(
            namedtype.NamedType('accessMethod', univ.ObjectIdentifier()),
            namedtype.NamedType('accessLocation', rfc5280.GeneralName()))

    class AuthorityInfoAccessSyntax(univ.SequenceOf):
        componentType = AccessDescription()

    # looping over certificate extensions
    for seq in decoder.decode(cert)[0][0][7]:
        if str(seq[0]) == '1.3.6.1.5.5.7.1.1':  # look for AIA extension
            ext_value = bytes(seq[1])
            for aia in decoder.decode(ext_value, asn1Spec=AuthorityInfoAccessSyntax())[0]:
                if str(aia[0]) == '1.3.6.1.5.5.7.48.2':  # ca url
                    return str(aia[1].getComponentByName('uniformResourceIdentifier'))
    print("[-] CA url not found in certificate!")
    exit(1)


def download_issuer_cert(issuer_cert_url):
    print("[+] Downloading issuer certificate from:", issuer_cert_url)
    # parse ocsp responder url
    url = urlparse(issuer_cert_url).netloc
    getURL = url + urlparse(issuer_cert_url).path
    # connect to host
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((url, 80))
    # sent HTTP GET request
    s.send(b'GET / HTTP/1.1\r\nHost: ' + getURL.encode() + b'\r\n\r\n')
    # read HTTP response header
    header = bytes()
    while header[-4:] != b'\r\n\r\n':
        header += s.recv(1)
    # get HTTP response length
    bodyLength = re.search('content-length:\s*(\d+)\s', header.decode(), re.S + re.I).group(1)
    # read HTTP response body
    issuer_cert = bytes()
    for i in range(int(bodyLength)):
        issuer_cert += s.recv(1)
    return issuer_cert


# parses OCSP response
def parse_ocsp_resp(ocsp_resp):
    ocspResponse, _ = decoder.decode(ocsp_resp, asn1Spec=rfc2560.OCSPResponse())
    responseStatus = ocspResponse.getComponentByName('responseStatus')
    assert responseStatus == rfc2560.OCSPResponseStatus('successful'), responseStatus.prettyPrint()
    responseBytes = ocspResponse.getComponentByName('responseBytes')
    responseType = responseBytes.getComponentByName('responseType')
    assert responseType == rfc2560.id_pkix_ocsp_basic, responseType.prettyPrint()
    response = responseBytes.getComponentByName('response')
    basicOCSPResponse, _ = decoder.decode(
        response, asn1Spec=rfc2560.BasicOCSPResponse()
    )
    tbsResponseData = basicOCSPResponse.getComponentByName('tbsResponseData')
    response0 = tbsResponseData.getComponentByName('responses').getComponentByPosition(0)
    producedAt = datetime.datetime.strptime(str(tbsResponseData.getComponentByName('producedAt')), '%Y%m%d%H%M%SZ')
    certID = response0.getComponentByName('certID')
    certStatus = response0.getComponentByName('certStatus').getName()
    thisUpdate = datetime.datetime.strptime(str(response0.getComponentByName('thisUpdate')), '%Y%m%d%H%M%SZ')
    # let's assume that certID in response matches the certID sent in the request
    # let's assume that response signed by trusted responder
    print("[+] OCSP producedAt:", producedAt)
    print("[+] OCSP thisUpdate:", thisUpdate)
    print("[+] OCSP status:", certStatus)


cert = pem_to_der(open(sys.argv[1], 'rb').readlines())
ocsp_url = get_ocsp_url(cert)
print("[+] URL of OCSP responder:", ocsp_url)
issuer_cert_url = get_issuer_cert_url(cert)
issuer_cert = download_issuer_cert(issuer_cert_url)
ocsp_req = produce_request(cert, issuer_cert)
ocsp_resp = send_req(ocsp_req, ocsp_url)
parse_ocsp_resp(ocsp_resp)
